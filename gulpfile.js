let gulp = require('gulp');
let gutil = require('gulp-util');
let notify = require("gulp-notify");
let onError = function (err) {
  notify({
    title: 'Gulp Task Error',
    message: 'Check the console.'
  }).write(err);
  console.log(err.toString());
  this.emit('end');
};

// ========== CONFIG ==========
let env = require('gulp-env');
env({
    file: '.env',
    type: 'ini',
});
const sitePort = 8000;
const siteProxy = process.env.PROJECT_BASE_URL + ':' + sitePort;
const webRoot = './web';
const themeName = process.env.PROJECT_NAME;
const siteDir = webRoot + '/sites/default';
const themeDir = webRoot + '/themes/custom/' + themeName;
const buildRoot = themeDir + '';
const srcRoot = themeDir + '/assets';
let path = {
  src: {
    img: srcRoot + '/images/**/*',
    js: [
      srcRoot + '/js/**/*.js',
      'node_modules/bootstrap/dist/js/bootstrap.js',
//      'node_modules/mdbootstrap/js/mdb.min.js',
      'node_modules/popper.js/dist/umd/popper.js'
    ],
    scss: [
      // 'node_modules/bootstrap/scss/bootstrap.scss', 
//      'node_modules/mdbootstrap/scss/mdb.scss'
      srcRoot + '/scss/**/style.scss',
    ]
  },
  build: {
    img: buildRoot + '/images',
    js: buildRoot + '/js',
    css: buildRoot + '/css'
  },
  watch: {
    reload: themeDir + '/**/*',
    js: srcRoot + '/js/**/*.js',
    scss: srcRoot + '/scss/**/*.scss',
    img: srcRoot + '/images/**/*'
  }
};


// ========== Images ==========
const image = require('gulp-image');

gulp.task('image', function () {
  gulp.src(path.src.img)
      .pipe(image())
      .pipe(gulp.dest(path.build.img));
});


// ========== JS ==========

gulp.task('js', function () {
  return gulp.src(path.src.js)
      .on('error', onError)
      .pipe(gulp.dest(path.build.js))
      .pipe(reload({stream: true})); //И перезагрузим сервер
});


// ========== SCSS ==========
let sass = require('gulp-sass');
let sourcemaps = require('gulp-sourcemaps');
let prefixer = require('gulp-autoprefixer');

gulp.task('scss', function () {
  console.log(path.src.scss);
  return gulp.src(path.src.scss)
      .pipe(sourcemaps.init())
      .pipe(sass({
        // outputStyle: 'compressed',
        includePaths: ['node_modules/bootstrap/scss/']
      }))
      .on('error', onError)
//      .pipe(prefixer())
      .pipe(sourcemaps.write())
      .pipe(gulp.dest(path.build.css))
      .pipe(reload({stream: true})); //И перезагрузим сервер
});

// ========== WATCH ==========
let watch = require('gulp-watch');
gulp.task('watch', function () {
  watch([path.watch.reload], reload);
  watch([path.watch.img], function (event, cb) {
    gulp.start('image');
  });
  watch([path.watch.scss], function (event, cb) {
    gulp.start('scss');
  });
  watch([path.watch.js], function (event, cb) {
    gulp.start('js');
  });
});

gulp.task('build', [
  'image',
  'js',
  'scss'
]);

// ======= BrowserSync ========
let browserSync = require("browser-sync");
let reload = browserSync.reload;
let config = {
  // server: {
  //     baseDir: buildRoot
  // },
  proxy: siteProxy,
  online: true,
  // tunnel: true,
  // host: 'localhost',
  // port: 9090,
  logPrefix: "Frontend_Devil"
};
gulp.task('webserver', function () {
  browserSync(config);
});

gulp.task('default', [
  'build',
  'webserver',
  'watch'
]);
