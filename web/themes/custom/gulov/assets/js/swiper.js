(function ($) {
    "use strict";

    /*----------------------------
        service swiper active
    ------------------------------ */
    var mySwiper = new Swiper('.swiper-container.swiper-service', {
        init: true,
        speed: 200,
        loop: true,
        slidesPerView: 3,
        spaceBetween: 30,
        navigation: {
            nextEl: '.service_next_btn',
            prevEl: '.service_prev_btn'
        },
        breakpoints: {
            760: {
                slidesPerView: 1
            },
            990: {
                slidesPerView: 2
            },
            1000: {
                slidesPerView: 3
            }
        }
    });

    /*----------------------------
        testimonial swiper active
    ------------------------------ */
    var mySwiperc = new Swiper('.swiper-container.swiper-testimonial', {
        speed: 1500,
        loop: true,
        autoHeight: true,
        autoplay: {
            disableOnInteraction: false
        },
        navigation: {
            nextEl: '.testimonial_next_btn',
            prevEl: '.testimonial_prev_btn'
        }
    });

    /*----------------------------
        work-details swiper active
    ------------------------------ */
    var mySwipera = new Swiper('.swiper-container.swiper-work-details', {
        speed: 500,
        loop: true,
        autoHeight: true,
        autoplay: {
            disableOnInteraction: false
        },
        navigation: {
            nextEl: '.work_details_next_btn',
            prevEl: '.work_details_prev_btn'
        }
    });

    /*----------------------------
        index_02 landing_slides swiper active
    ------------------------------ */
    var mySwipere = new Swiper('.swiper-container.landing_slides', {
        speed: 500,
        loop: true,
        autoplay: {
            disableOnInteraction: false,
            delay: 5000
        },
        navigation: {
            nextEl: '.landing_slides_next_btn',
            prevEl: '.landing_slides_prev_btn'
        },
        on: {
            init: function () {
                swiperAnimateCache(this);
                swiperAnimate(this);
            },
            slideChangeTransitionEnd: function () {
                swiperAnimate(this);
            }
        }
    });
})(jQuery);
