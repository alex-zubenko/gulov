(function ($) {
    "use strict";

    /*----------------------------
        fixed menu active
    ------------------------------ */
    // $(window).on('scroll', function () {
    //     if ($(window).scrollTop() >= 500) {
    //         $('nav').addClass('nav_shadow');
    //         return false;
    //     } else {
    //         $('nav').removeClass('nav_shadow');
    //         return false;
    //     }
    // });

    /*----------------------------
        responsive menu active
    ------------------------------ */
    // $('.menu_toggle').on('click', function () {
    //     $('.header_menu').addClass('menu_show');
    //     $('body').css('overflow-y', 'hidden');
    //     return false;
    // });
    // $('.menu_close').on('click', function () {
    //     $('.header_menu').removeClass('menu_show');
    //     $('body').css('overflow-y', 'scroll');
    //     return false;
    // });
    // $('.drop_menu').on('click', function () {
    //     $('.header_menu>ul>li>ul').hide();
    //     $(this, '.drop_menu').siblings('ul').show();
    //     return false;
    // });

    /*----------------------------
        smooth scroll active
    ------------------------------ */
    $('a').on('click', function () {
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top
        }, 500);
        return false;
    });

    /*----------------------------
        animsition
    ------------------------------ */
    $(".animsition").animsition();

})(jQuery);
