'use strict';
var npmRoot = '../../../../node_modules/';
var gulp = require(npmRoot + 'gulp');
var sass = require(npmRoot + 'gulp-sass');

gulp.task('sass', function () {
  return gulp.src('./assets/scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
});

gulp.task('sass:watch', function () {
  gulp.watch('./assets/scss/**/*.scss', ['sass']);
});
